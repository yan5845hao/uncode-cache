package cn.uncode.cache.springboot;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.uncode.cache.annotation.Cache;

/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @auth:qiss
 * @see: [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@RestController
@RequestMapping("/uncode/cache")
public class CacheController {
    @RequestMapping(value = "/test")
    @Cache(key = "1001",expired =1000000000 )
    public String uncodeCache() {
        return "cache test";
    }
}
