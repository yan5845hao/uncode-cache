package cn.uncode.cache.subscribe;

import java.util.ArrayList;
import java.util.List;

public class EventSubscribeFactory {
	
	private static final List<EventSubscribe> ALL =  new ArrayList<>();
	
	private EventSubscribeFactory(){}
	
	public static void register(EventSubscribe eventSubscribe){
		ALL.add(eventSubscribe);
	}
	
	public static List<EventSubscribe> getSubscribes(){
		return ALL;
	}

}
