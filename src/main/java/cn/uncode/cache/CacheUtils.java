package cn.uncode.cache;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cn.uncode.cache.framework.ICache;
import cn.uncode.cache.framework.util.CacheCodeUtil;
import cn.uncode.cache.framework.util.SerializeUtil;
import cn.uncode.cache.store.CacheStore;
import cn.uncode.cache.store.redis.cluster.JedisClusterCustom;
import cn.uncode.cache.store.redis.lock.DistributeLock;
import cn.uncode.cache.store.redis.lock.DistributeLockFactory;

/**
 * 缓存工具类 【重新整理】<br/>
 *
 * 请不要随意添加方法,不能满足需求请先沟通
 *
 * @author juny.ye
 *
 */
public class CacheUtils {

	private static final Log LOG = LogFactory.getLog(CacheUtils.class);

	private static ICache<Object, Object> cache;

	// *******************************************************************
	// 对外暴露接口
	// *******************************************************************
	/**
	 * 获取存储器【对外统一接口，推荐使用】
	 *
	 * @return 统一缓存接口
	 */
	@SuppressWarnings("unchecked")
	public static ICache<Object, Object> getCache() {
		if (cache == null) {
			if (CacheManager.getApplicationContext() != null) {
				CacheUtils.cache = (ICache<Object, Object>) CacheManager
						.getApplicationContext().getBean(ICache.class);
			}
		}
		cache.setStoreRegion(CacheManager.getStoreRegion());
		return CacheUtils.cache;
	}

	public static ICache<Object, Object> getCache(String storeRegion) {
		CacheUtils.cache = getCache();
		CacheUtils.cache.setStoreRegion(storeRegion);
		return CacheUtils.cache;
	}

	/**
	 * 【谨慎使用，底层框架可以使用，业务层不能使用】获取redis数据源
	 *
	 * @return redisCluster
	 */
	public static JedisClusterCustom getRedisCache() {
		JedisClusterCustom jedisClusterCustom = CacheManager
				.getApplicationContext().getBean(JedisClusterCustom.class);
		if (null == jedisClusterCustom)
			LOG.debug("Redis instance is null.");
		return jedisClusterCustom;
	}

	/**
	 * 获取redis分布式锁，异常情况下自已释放时间2*1000毫秒<br/>
	 * 建议使用DistributeLockFactory.getLock(key)<br/>
	 * 示例：
	 *
	 * <pre>
	 *  key必须全局唯一
	 *  String key = "alsdkfe3223lksdja932er";
	 *  DistributeLock lock = CacheUtils.getDistributeLock(key);
	 *  try {
	 *     if(lock.lock()){
	 *     	  //拿到锁业务逻辑处理
	 *        try {
	 *            业务处理逻辑
	 *        } catch (InterruptedException e) {
	 *               e.printStackTrace();
	 *        }
	 *     } else {
	 * 	  //没有锁到锁的处理
	 *     }
	 *   } finally {
	 *       lock.unlock();
	 *   }
	 * </pre>
	 *
	 * @param key
	 *            加锁的关键标识
	 * @return 分布式锁
	 */
	public static DistributeLock getDistributeLock(String key) {
		return DistributeLockFactory.getLock(key);
	}

	// *******************************************************************
	// 以下方法不推荐使用，逐步丢弃
	// *******************************************************************

	/**
	 * 设置缓存对应的实现
	 *
	 * @param cache
	 */
	public static void setCache(ICache<Object, Object> cache) {
		CacheUtils.cache = cache;
	}

	// *******************************************************************
	// 以下方法为redis特性方法，已重新整理，不要随意添加，需要添加提前沟通
	// *******************************************************************
	/**
	 * 【LIST】向list添加数据，非序列化保存操作
	 *
	 * @param listKey
	 *            list的key
	 * @param value
	 *            值
	 */
	public static void listAdd(String listKey, String value) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			listKey = CacheCodeUtil.getCacheAdapterKey(storeRegion, listKey);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			jedisCluster.rpush(listKey, value);
			LOG.debug("[listAdd] redis cache write,key:" + listKey + ",value:"
					+ value);
		} catch (Exception e) {
			LOG.error("[listAdd] redis cache error", e);
		} finally {
			// jedisCluster.close();
		}
	}

	/**
	 * 【LIST】从list中取出指定范围的值
	 *
	 * @param listKey
	 *            list的key
	 * @param start
	 *            起始位置
	 * @param end
	 *            结束位置
	 * @return 指定范围的值
	 */
	public static List<String> listGet(String listKey, int start, int end) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			listKey = CacheCodeUtil.getCacheAdapterKey(storeRegion, listKey);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			List<String> values = jedisCluster.lrange(listKey, start, end);
			LOG.debug("[listGet] redis cache read,start:" + start + ",end:"
					+ end + ",key:"+ listKey + ",value:" + values);
			return values;
		} catch (Exception e) {
			LOG.error("[listGet] redis cache error", e);
		} finally {
			// jedisCluster.close();
		}
		return null;
	}

	/**
	 * 【LIST】从list中取出所有值
	 *
	 * @param listKey
	 *            list的key
	 * @return 所有值
	 */
	public static List<String> listGet(String listKey) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			listKey = CacheCodeUtil.getCacheAdapterKey(storeRegion, listKey);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			List<String> values = jedisCluster.lrange(listKey, 0, -1);
			LOG.debug("[listGet] redis cache read,key:"+listKey + ",value:"
					+ values);
			return values;
		} catch (Exception e) {
			LOG.error("[listGet] redis cache error", e);
		} finally {
			// jedisCluster.close();
		}
		return null;
	}

	/**
	 * 【LIST】取list的大小
	 *
	 * @param listKey
	 *            list的key
	 * @return 大小
	 */
	public static long listSize(String listKey) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			listKey = CacheCodeUtil.getCacheAdapterKey(storeRegion, listKey);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			long size = jedisCluster.llen(listKey);
			LOG.debug("[listSize] redis cache read,key:"+ listKey + ",size:"
					+ size);
			return size;
		} catch (Exception e) {
			LOG.error("[listSize] redis cache error", e);
		} finally {
			// jedisCluster.close();
		}
		return 0;
	}

	/**
	 * 【LIST】对list进行排序
	 *
	 * @param listKey
	 *            list的key
	 * @return 排序后的结果
	 */
	public static List<String> listSort(String listKey) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			listKey = CacheCodeUtil.getCacheAdapterKey(storeRegion, listKey);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			List<String> result = jedisCluster.sort(listKey);
			LOG.debug("[listSort] redis cache read,key:"+ listKey + ",result:"
					+ result);
			return result;
		} catch (Exception e) {
			LOG.error("[listSort] redis cache error", e);
		} finally {
			// jedisCluster.close();
		}
		return null;
	}

	/**
	 * 【LIST】修改list中单个下标的值
	 *
	 * @param listKey
	 *            list的key
	 * @param index
	 *            下标
	 * @param value
	 *            值
	 * @return 排序后的结果
	 */
	public static List<String> listSet(String listKey, int index, String value) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			listKey = CacheCodeUtil.getCacheAdapterKey(storeRegion, listKey);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			jedisCluster.lset(listKey, index, value);
			LOG.debug("[listSet] redis cache write,key:"+ listKey + ".index:"
					+ index + ",result:" + value);
		} catch (Exception e) {
			LOG.error("[listSet] redis cache error", e);
		} finally {
			// jedisCluster.close();
		}
		return null;
	}

	/**
	 * 【LIST】获取list指定下标的值
	 *
	 * @param listKey
	 *            list的key
	 * @param index
	 *            下标
	 * @return 指定下标的值
	 */
	public static String listIndex(String listKey, int index) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			listKey = CacheCodeUtil.getCacheAdapterKey(storeRegion, listKey);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			String result = jedisCluster.lindex(listKey, index);
			LOG.debug("[listIndex] redis cache read,key:"+ listKey + ".index:"
					+ index + ",result:" + result);
			return result;
		} catch (Exception e) {
			LOG.error("[listIndex] redis cache error", e);
		} finally {
			// jedisCluster.close();
		}
		return null;
	}

	/**
	 * 【LIST】list出栈 ,返回key为listkey的列表的头元素，并删除该元素
	 *
	 * @param listKey
	 *            list的key
	 * @return 值
	 */
	public static String listPop(String listKey) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			listKey = CacheCodeUtil.getCacheAdapterKey(storeRegion, listKey);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			String result = jedisCluster.lpop(listKey);
			LOG.debug("[listPop] redis cache read,key:"+ listKey + ",result:"
					+ result);
			return result;
		} catch (Exception e) {
			LOG.error("[listPop] redis cache error", e);
		} finally {
			// jedisCluster.close();
		}
		return null;
	}

	/**
	 * 【LIST】根据参数count的值，移除列表中与参数value相等的元素
	 *
	 * <pre>
	 * COUNT 的值可以是以下几种：
	 * count > 0 : 从表头开始向表尾搜索，移除与 value相等的元素，数量为 count。
	 * count < 0 : 从表尾开始向表头搜索，移除与 value相等的元素，数量为 count的绝对值。
	 * count = 0 : 移除表中所有与 value相等的值。
	 * </pre>
	 *
	 * @param listKey
	 *            list的key
	 * @param count
	 *            操作类型
	 * @param value
	 *            元素值
	 * @return 移除的数量
	 */
	public static long listRemove(String listKey, int count, String value) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			listKey = CacheCodeUtil.getCacheAdapterKey(storeRegion, listKey);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			LOG.debug("[listRemove] redis cache write,key:"+ listKey
					+ ",count:" + count + ",value:" + value);
			return jedisCluster.lrem(listKey, count, value);
		} catch (Exception e) {
			LOG.error("[listRemove] redis cache error", e);
		} finally {
			// jedisCluster.close();
		}
		return 0;
	}

	/**
	 * 【list&set&map】删除整个list/map/set/all
	 *
	 * @param key
	 *            缓存key
	 * @return 2017年12月1日
	 * @author jason.li
	 */
	public static long del(String key) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			key = CacheCodeUtil.getCacheAdapterKey(storeRegion, key);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			LOG.debug("[del] redis cache write,key:"+ key);
			return jedisCluster.del(key);
		} catch (Exception e) {
			LOG.error("[del] redis cache error", e);
		} finally {
			// jedisCluster.close();
		}
		return 0;
	}

	/**
	 * 【SET】向set添加数据
	 *
	 * @param setKey
	 *            set的key
	 * @param value
	 *            值
	 */
	public static void setAdd(String setKey, String value) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			setKey = CacheCodeUtil.getCacheAdapterKey(storeRegion, setKey);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			jedisCluster.sadd(setKey, value);
			LOG.debug("[setAdd] redis cache write,key:"+ setKey + ",value:"
					+ value);
		} catch (Exception e) {
			LOG.error("[setAdd] redis cache error", e);
		} finally {
			// jedisCluster.close();
		}
	}

	/**
	 * 【SET】从set中取出所有值
	 *
	 * @param setKey
	 *            set的key
	 * @return 所有值
	 */
	public static Set<String> setGet(String setKey) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			setKey = CacheCodeUtil.getCacheAdapterKey(storeRegion, setKey);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			Set<String> result = jedisCluster.smembers(setKey);
			LOG.debug("[setGet] redis cache read,key:"+setKey + ",value:"
					+ result);
			return result;
		} catch (Exception e) {
			LOG.error("[setGet] redis cache error", e);
		} finally {
			// jedisCluster.close();
		}
		return null;
	}

	/**
	 * 【SET】取set的大小
	 *
	 * @param setKey
	 *            set的key
	 * @return 大小
	 */
	public static long setSize(String setKey) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			setKey = CacheCodeUtil.getCacheAdapterKey(storeRegion, setKey);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			return jedisCluster.scard(setKey);
		} catch (Exception e) {
			LOG.error("[setSize] redis cache error", e);
		} finally {
			/* pool.returnResource(jedisCluster); */
		}
		return 0;
	}

	/**
	 * 【SET】setKey是否存在
	 *
	 * @param setKey
	 *            set的key
	 * @return 结果
	 */
	public static boolean setExists(String setKey, String value) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			setKey = CacheCodeUtil.getCacheAdapterKey(storeRegion, setKey);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			return jedisCluster.sismember(setKey, value);
		} catch (Exception e) {
			LOG.error("[setExists] redis cache error", e);
		} finally {
			// jedisCluster.close();
		}
		return false;
	}

	/**
	 * 【SET】set出栈
	 *
	 * @param setKey
	 * @return 值
	 */
	public static String setPop(String setKey) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			setKey = CacheCodeUtil.getCacheAdapterKey(storeRegion, setKey);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			return jedisCluster.spop(setKey);
		} catch (Exception e) {
			LOG.error("[setPop] redis cache error", e);
		} finally {
			// jedisCluster.close();
		}
		return null;
	}

	/**
	 * 【SET】删除指定的值
	 *
	 * @param setKey
	 * @return
	 */
	public static long setRemove(String setKey) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			setKey = CacheCodeUtil.getCacheAdapterKey(storeRegion, setKey);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			return jedisCluster.srem(setKey);
		} catch (Exception e) {
			LOG.error("[setRemove] redis cache error", e);
		} finally {
			// jedisCluster.close();
		}
		return 0;
	}

	/**
	 * 【MAP】保存map,批量保存
	 *
	 * @param mapKey
	 *            map的key
	 * @param map
	 *            值
	 */
	public static void mapSave(String mapKey, Map<String, String> map) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			mapKey = CacheCodeUtil.getCacheAdapterKey(storeRegion, mapKey);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			jedisCluster.hmset(mapKey, map);
		} catch (Exception e) {
			LOG.error("[mapSave] redis cache error", e);
		} finally {
			// jedisCluster.close();
		}
	}

	/**
	 * 【MAP】map存数据 ,单个保存
	 *
	 * @param mapKey
	 *            map的key
	 * @param key
	 *            键
	 * @param value
	 *            值
	 */
	public static void mapPut(String mapKey, String key, String value) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			mapKey = CacheCodeUtil.getCacheAdapterKey(storeRegion, mapKey);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			jedisCluster.hset(mapKey, key, value);
		} catch (Exception e) {
			LOG.error("[mapPut] redis cache error", e);
		} finally {
			// jedisCluster.close();
		}
	}

	/**
	 * 【MAP】map的key是否存在
	 *
	 * @param mapKey
	 *            map的key
	 * @param key
	 *            键
	 */
	public static void mapExists(String mapKey, String key) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			mapKey = CacheCodeUtil.getCacheAdapterKey(storeRegion, mapKey);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			jedisCluster.hexists(mapKey, key);
		} catch (Exception e) {
			LOG.error("[mapExists] redis cache error", e);
		} finally {
			// jedisCluster.close();
		}
	}

	/**
	 * 【MAP】获取map指定key的值
	 *
	 * @param mapKey
	 *            map的key
	 * @param key
	 *            键
	 */
	public static String mapGet(String mapKey, String key) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			mapKey = CacheCodeUtil.getCacheAdapterKey(storeRegion, mapKey);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			return jedisCluster.hget(mapKey, key);
		} catch (Exception e) {
			LOG.error("[mapGet] redis cache error", e);
		} finally {
			// jedisCluster.close();
		}
		return null;
	}

	/**
	 * 【MAP】获取map所有keys
	 *
	 * @param mapKey
	 *            map的key
	 */
	public static Set<String> mapKeys(String mapKey) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			mapKey = CacheCodeUtil.getCacheAdapterKey(storeRegion, mapKey);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			return jedisCluster.hkeys(mapKey);
		} catch (Exception e) {
			LOG.error("[mapKeys] redis cache error", e);
		} finally {
			// jedisCluster.close();
		}
		return null;
	}

	/**
	 * 【MAP】获取map所有values
	 *
	 * @param mapKey
	 *            map的key
	 */
	public static List<String> mapValues(String mapKey) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			mapKey = CacheCodeUtil.getCacheAdapterKey(storeRegion, mapKey);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			return jedisCluster.hvals(mapKey);
		} catch (Exception e) {
			LOG.error("[mapValues] redis cache error", e);
		} finally {
			// jedisCluster.close();
		}
		return null;
	}

	/**
	 * 【MAP】获取map的个数
	 *
	 * @param mapKey
	 *            map的key
	 */
	public static Long mapSize(String mapKey) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			mapKey = CacheCodeUtil.getCacheAdapterKey(storeRegion, mapKey);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			return jedisCluster.hlen(mapKey);
		} catch (Exception e) {
			LOG.error("[mapSize] redis cache error", e);
		} finally {
			// jedisCluster.close();
		}
		return null;
	}

	/**
	 * 【MAP】获取整个map的值
	 *
	 * @param mapKey
	 *            map的key
	 */
	public static Map<String, String> mapGetAll(String mapKey) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			mapKey = CacheCodeUtil.getCacheAdapterKey(storeRegion, mapKey);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			return jedisCluster.hgetAll(mapKey);
		} catch (Exception e) {
			LOG.error("[mapGetAll] redis cache error", e);
		} finally {
			// jedisCluster.close();
		}
		return null;
	}

	/**
	 * 【MAP】删除map中某个键的值
	 *
	 * @param mapKey
	 *            map的key
	 */
	public static Long mapRemove(String mapKey, String... keys) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			mapKey = CacheCodeUtil.getCacheAdapterKey(storeRegion, mapKey);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {

			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			return jedisCluster.hdel(mapKey, keys);
		} catch (Exception e) {
			LOG.error("[mapRemove] redis cache error", e);
		} finally {
			// jedisCluster.close();
		}
		return 0L;

	}

	/**
	 * 发布订阅机制之发布消息
	 *
	 * @param subscribeClass
	 * @param message
	 * @return
	 */
/*	public static Long publish(Class<?> subscribeClass, String message) {
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			return jedisCluster.publish(EventSubscribe.KEY,
					subscribeClass.getCanonicalName() + EventSubscribe.SPLITCHAR
							+ message);
		} catch (Exception e) {
			LOG.error("redis publish error", e);
		} finally {
		}
		return 0l;
	}*/

	public static void sendEvent(String topic, String message){
		CacheStore cacheStore = (CacheStore) getCache();
		cacheStore.getCacheTemplate().sendEvent("_uncode_custom_event", topic, message);
	}

	/**
	 * 【LIST】list出栈 ,返回key为listkey的列表的头元素，并删除该元素。
	 *
	 * @param listKey
	 *            list的key,会序列化为byte数组
	 * @return 返回反序列化的对象类型
	 */
	public static Object listPopObject(String listKey) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			listKey = CacheCodeUtil.getCacheAdapterKey(storeRegion, listKey);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			byte[] key = SerializeUtil.serialize(listKey);
			byte[] result = jedisCluster.lpop(key);
			LOG.debug("[listPopObject] redis cache read,key:"+ listKey
					+ ",result:" + result);
			return SerializeUtil.unserialize(result);
		} catch (Exception e) {
			LOG.error("[listPopObject] redis cache error", e);
		} finally {
			// jedisCluster.close();
		}
		return null;
	}

	/**
	 * 【LIST】批量向list添加数据 --- 针对key进行序列化后保存
	 *
	 * @param listKey
	 *            list的key,会序列化为byte数组
	 * @param value
	 *            值
	 */
	public static void listAdd(String listKey, byte[]... value) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			listKey = CacheCodeUtil.getCacheAdapterKey(storeRegion, listKey);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			byte[] key = SerializeUtil.serialize(listKey);
			jedisCluster.rpush(key, value);
			LOG.debug("[listAdd] redis cache write,key:"+ listKey + ",value:"
					+ value);
		} catch (Exception e) {
			LOG.error("[listAdd] redis cache error", e);
		} finally {
		}
	}

	/**
	 * key自增
	 *
	 * @param key
	 * @return Long
	 */
	public static long incr(Object key) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			key = CacheCodeUtil.getCacheAdapterKey(storeRegion, key);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			return jedisCluster.incr(SerializeUtil.serialize(key));
		} catch (Exception e) {
			LOG.error("[incr] redis cache error, key" + key + " value:"
					+ jedisCluster.get(SerializeUtil.serialize(key)), e);
		} finally {
		}
		return 0l;
	}

	/**
	 * 【LIST】返回名称为listKey的list中index位置的元素
	 *
	 * @param listKey
	 *            list的key,会序列化为byte数组
	 * @param index
	 *            下标
	 * @author meff
	 * @return 返回反序列化的对象类型
	 */
	public static Object listIndex(String listKey, long index) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			listKey = CacheCodeUtil.getCacheAdapterKey(storeRegion, listKey);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			byte[] key = SerializeUtil.serialize(listKey);
			byte[] result = jedisCluster.lindex(key, index);
			return SerializeUtil.unserialize(result);
		} catch (Exception e) {
			LOG.error("[listIndex] redis cache error", e);
		} finally {
		}
		return null;
	}

	/**
	 * 【LIST】删除键为key的list中值为value的元素。count为0，删除所有值为value的元素
	 *
	 * @param listKey
	 *            list的key,会序列化为byte数组
	 * @param object
	 *            元素值
	 * @return 移删的数量
	 */
	public static long listRemoveObject(String listKey, Object object) {
		String storeRegion = CacheManager.getStoreRegion();
		if (StringUtils.isNotBlank(storeRegion)) {
			listKey = CacheCodeUtil.getCacheAdapterKey(storeRegion, listKey);
		}
		JedisClusterCustom jedisCluster = CacheUtils.getRedisCache();
		try {
			if (jedisCluster == null) {
				throw new Exception("jedisCluster is null.");
			}
			byte[] key = SerializeUtil.serialize(listKey);
			byte[] value = SerializeUtil.serialize(object);
			return jedisCluster.lrem(key, 1, value);
		} catch (Exception e) {
			LOG.error("[listRemoveObject] redis cache error", e);
		} finally {
		}
		return 0l;
	}

}
