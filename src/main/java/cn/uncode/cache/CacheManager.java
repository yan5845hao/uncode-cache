package cn.uncode.cache;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEvent;



/**
 * 
 * @author juny.ye
 * @email  juny.ye@ksudi.com
 *
 * 2015年4月24日
 */
public class CacheManager<E extends ApplicationEvent> implements ApplicationContextAware{

    
    protected static ApplicationContext applicationContext;
    /**
     * 缓存分区（可选）
     */
    private static String storeRegion;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    	CacheManager.applicationContext = applicationContext;
    }

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

	public static String getStoreRegion() {
		return storeRegion;
	}

	public void  setStoreRegion(String storeRegion) {
		CacheManager.storeRegion = storeRegion;
	}

    
    

}
