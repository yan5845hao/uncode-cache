package cn.uncode.cache.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import cn.uncode.cache.CacheUtils;
import cn.uncode.cache.framework.Level;




@WebServlet(name="cache",urlPatterns="/uncode/cache")
public class ManagerServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8160082230341182715L;

	private static final String UNCODE_SESSION_KEY = "uncode_key_session";
	
	private static final String HEAD = 
		    "<!DOCTYPE html>\n"+
		    "<html>\n"+
		    "<head>\n"+
		    "<meta charset=\"utf-8\"/>\n"+
		    "\t  <title>Uncode-Cache管理</title>\n"+
		    "\t  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"+
		    "\t  <meta name=\"viewport\" content=\"width=device-width\"/>\n"+
		    "\t  <meta name=\"keywords\" content=\"uncode,冶卫军\"/>\n"+
		    "\t  <meta name=\"description\" content=\"Uncode-Cache管理\"/>\n"+
		    "\t  <link rel=\"stylesheet\"  href=\"http://cdn.bootcss.com/bootstrap/3.3.4/css/bootstrap.min.css\">\n"+
		    "\t  <script type=\"text/javascript\" src=\"http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js\"></script>\n"+
		    "\t  <script type=\"text/javascript\" src=\"http://cdn.bootcss.com/bootstrap/3.3.4/js/bootstrap.min.js\"></script>\n"+
			"</head>\n";
	
	private static final String SCRIPT = 
			"\t	<script type=\"text/javascript\">\n"+
			"\t		$(document).ready(function(){\n"+
			"\t			$(\"#myModal\").on('show.bs.modal', function(event){\n"+
			"\t		    var button = $(event.relatedTarget); \n"+
			"\t			var titleData = button.data('title'); \n"+
			"\t		    var modal = $(this)\n"+
			"\t	       	modal.find('.modal-title').text(titleData + '定时任务');\n"+
			"\t	  		});\n"+
			"\t		});\n"+
			"\t		function formSubmit(){\n"+
			"\t			document.getElementById(\"cacheform\").submit();\n"+
			"\t		}\n"+
			"\t		function detail(akey){\n"+
			"\t			var key = $(akey).html();\n"+
			"\t			$(\"#key\").val(key);\n"+
			"\t			document.getElementById(\"cacheform\").submit();\n"+
			"\t		}\n"+
			"\t		function del(){\n"+
			"\t			$(\"#del\").val(1);\n"+
			"\t			document.getElementById(\"cacheform\").submit();\n"+
			"\t		}\n"+
			"\t	</script>";
	private static final String SCRIPT_LOGIN = 
			"\t	<script type=\"text/javascript\">\n"+
			"\t		function loginSubmit(){\n"+
			"\t		    var accout = $(\"#account\").val(); \n"+
			"\t		    var password = $(\"#password\").val(); \n"+
			"\t		    if(accout == null || accout.length == 0){ \n"+
			"\t		    	alert(\"用户名不能为空\"); \n"+
			"\t		    	return; \n"+
			"\t		    	return; \n"+
			"\t		    } \n"+
			"\t		    if(password == null || password.length == 0){ \n"+
			"\t		    	alert(\"密码不能为空\"); \n"+
			"\t		    	return; \n"+
			"\t		    } \n"+
			"\t			document.getElementById(\"loginform\").submit();\n"+
			"\t		}\n"+
			"\t	</script>";
	
	private static final String PAGE_LOGIN_STYLE = 
			"\t <style>	\n"+
			"\t ul{\n"+
			"\t 	list-style-type: none;\n"+
			"\t }\n"+
			"\t a, button {\n"+
			"\t 	cursor: pointer; \n"+
			"\t }\n"+
			"\t .loginContDiv{\n"+
			"\t 	width: 400px;\n"+
			"\t 	height: 500px;\n"+
			"\t 	background-color:  -#f0f0f0;\n"+
			"\t 	margin: 10%  auto;\n"+
			"\t 	text-align: left;\n"+
			"\t }\n"+
			"\t .loginContUl{\n"+
			"\t 	width :80%;\n"+
			"\t 	margin: 0px  auto ;\n"+
			"\t }\n"+
			"\t .loginContLi{\n"+
			"\t 	width :100%;\n"+
			"\t 	margin: 0px auto 20px;\n"+
			"\t }\n"+
			"\t .loginContLi div{\n"+
			"\t 	width :100%;\n"+
			"\t }\n"+
			"\t .loginContLi div input{\n"+
			"\t 	width:296px;\n"+
			"\t 	height:30px;\n"+
			"\t }\n"+
			"\t .txt{\n"+
			"\t 	font-size:16px;\n"+
			"\t 	padding:  5px 10px ;\n"+
			"\t }\n"+
			"\t .loginBtn{\n"+
			"\t 	width :100%;\n"+
			"\t 	height: 42px;\n"+
			"\t 	border: 0;\n"+
			"\t 	border-bottom-style: hidden;\n"+
			"\t 	background-color:#84AF00;	\n"+
			"\t }\n"+
			"\t .loginBtn span{\n"+
			"\t 	color:white;\n"+
			"\t 	font-size: 20px;\n"+
			"\t 	line-height: 40px;\n"+
			"\t 	letter-spacing: 14px;\n"+
			"\t }\n"+
			"\t </style>\n";
			
	private static final String PAGE_LOGIN_HTML_1 = 
			"\t <body>\n"+
			"\t <script src='http://git.oschina.net/uncode/uncode-schedule/star_widget_preview'></script>"+
			"\t <div class=\"loginContDiv\">\n"+
			"\t 	<ul class=\"loginContUl\">\n"+
			"\t 		<li class=\"loginContLi\"> \n"+
			"\t 			<div style=\"text-align:center;height:90px;margin:0 auto;font-size: 250%;\">uncode-Cache</div>\n"+
			"\t 		</li>\n"+
			"\t 		<li class=\"loginContLi\"> \n"+
			"\t 			<div><form id=\"loginform\" method=\"post\" action=\"";

			
	private static final String PAGE_LOGIN_HTML_2 = 
			"\">\n"+
			"\t 				<input  type=\"text\" name =\"account\" id=\"account\" placeholder=\"帐号\" /><br/><br/>\n"+
			"\t 				<input  class=\"txt\" type=\"password\" name=\"password\" id=\"password\" placeholder=\"密码\" /><br/><br/>\n"+
			"\t 				<button type=\"button\" style=\"width:100px;\" onclick=\"loginSubmit()\">登录</button>\t"+
			"\t 				&nbsp;&nbsp;&nbsp;<a target=\"_blank\" href=\"http://git.oschina.net/uncode\">使用帮助</a>"+
			"\t 			</div></form>\n"+
			"\t 		</li>\n"+
			"\t 	</ul>\n"+
			"\t </div>\n"+



			"\t </body>";
	
	
	private static final String PAGE = 
			"\t <body>\n"+
			"\t <div class=\"container\">\n"+
			"\t     <h1>Uncode Cache Manager</h1>\n"+
			"\t     <div id=\"alert\" class=\"alert alert-success\" style=\"display: none\">\n"+
			"\t         <a href=\"#\" class=\"close\" onclick=\"$('#alert').hide();\">&times;</a> <strong>success!</strong>\n"+
			"\t     </div>\n"+
			"\t     <div class=\"row\">\n"+
			"\t         <hr>\n"+
			"\t 		<form id=\"cacheform\" method=\"post\" action=\"%s\">\n"+
			"\t         <div class=\"col-sm-4\">\n"+
			"\t             <div class=\"form-group\">\n"+
			"\t 				<b>Store Region:</b><select name=\"sr\" onchange=\"formSubmit()\">%s</select>\n"+
			"\t 				<input type=\"hidden\" id=\"key\"  name=\"key\" value=\"%s\" />\n"+
			"\t 				<input type=\"hidden\" id=\"del\"  name=\"del\" />\n"+
			"\t 				<b>Key Match:</b><input type=\"input\" id=\"match\"  name=\"match\" value=\"%s\" />&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"button\" onclick=\"formSubmit()\" value=\"查询"
			+ "\"> \n"+
			"\t             </div>\n"+
			"\t             <table class=\"table table-striped\" style=\"table-layout: fixed;\" id=\"keys\">\n"+
			"\t 	        	<tr>\n"+
			"\t 	            	<td><b>All Key Names:</b>%s</td>\n"+
			"\t 	            </tr>\n"+
			"\t 	        	<tr>%s</tr>\n"+
			"\t             </table>\n"+
			"\t         </div>\n"+
			"\t         </form>\n"+
			"\t         <div class=\"col-sm-8\">\n"+
			"\t         <div class=\"row\">\n"+
			"\t 	        <div class=\"col-sm-6\"  style=\"position:absolute;\">\n"+
			"\t 	        	<h2>Remote-redis</h2>\n"+
			"\t 				<textarea rows=\"30\" cols=\"100\" id=\"remote\">%s</textarea>\n"+
			"\t 	            <br/>\n"+
			"\t 	        </div>\n"+
			"\t 	    </div>\n"+
			"\t          </div>\n"+
			"\t     </div>\n"+
			"\t </div>\n"+
			"\t </body>";
	
	private Gson gson = new GsonBuilder().registerTypeAdapter(Timestamp.class,new TimestampTypeAdapter()).setDateFormat("yyyy-MM-dd HH:mm:ss").create();
	
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String login = (String) request.getSession().getAttribute(UNCODE_SESSION_KEY);
		response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();  
		if(StringUtils.isBlank(login)){
			String account = request.getParameter("account");
			String password = request.getParameter("password");
			boolean avilb = false;
			try {
				avilb = checkAdminUser(account, password);
				if(avilb){
					request.getSession().setAttribute(UNCODE_SESSION_KEY, "uncode_login_success");
					response.sendRedirect(request.getSession().getServletContext().getContextPath()+"/uncode/cache");
				}
			} catch (Exception e) {
			}
			out.write(HEAD); 
			out.write(SCRIPT_LOGIN);
			out.write(PAGE_LOGIN_STYLE);
			out.write(PAGE_LOGIN_HTML_1 + request.getSession().getServletContext().getContextPath()+"/uncode/cache" + PAGE_LOGIN_HTML_2);
		}else{
			String sr = request.getParameter("sr");
			String key = request.getParameter("key");
			String match = request.getParameter("match");
			String del = request.getParameter("del");
			
			Set<String> stores = CacheUtils.getCache().storeRegions(Level.Remote);
			StringBuffer sbTask = new StringBuffer();
    		for(String item:stores){
    			sbTask.append("<option value =\"").append(item);
    			if(StringUtils.isNotEmpty(sr) && item.equals(sr)){
    				sbTask.append("\" selected=\"selected\">");
    			}else{
    				sbTask.append("\">");
    			}
    			sbTask.append(item).append("</option>");
    		}
    		if(StringUtils.isEmpty(sr)){
    			sr = stores.iterator().next();
    		}
    		List<String> keysList = new ArrayList<String>();
    		StringBuffer keyStr = new StringBuffer();
    		if(StringUtils.isNotEmpty(sr)){
    			Set<String> keys = CacheUtils.getCache().storeRegionKeys(sr, Level.Remote);
    			if(null != keys){
    				for(String item:keys){
    					if(StringUtils.isNotBlank(match)) {
    						if(item.indexOf(match) != -1) {
    							keyStr.append("<tr class=\"info\">")
    	  		    			  .append("<td><a href=\"#\" onclick=\"detail(this)\">").append(item).append("</a></td>")
    	  		    			  .append("</tr>");
    							keysList.add(item);
    						}
    					}else {
    						keyStr.append("<tr class=\"info\">")
    		    			  .append("<td><a href=\"#\" onclick=\"detail(this)\">").append(item).append("</a></td>")
    		    			  .append("</tr>");
    					}
            		}
    			}
    		}
    		if(StringUtils.isNotBlank(del) && "1".equals(del)) {
    			for(String ikey:keysList) {
    				CacheUtils.getCache(sr).remove(ikey);
    			}
    		}
    		String kkey = "";
    		String valStr = "";
    		if(StringUtils.isNotEmpty(key)){
    			kkey = key;
    			Object value = CacheUtils.getCache(sr).get(kkey, Level.Remote);
    			valStr = gson.toJson(value);
    		}
    		String delHtml = "";
    		if(StringUtils.isNotBlank(match)) {
    			delHtml = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"#\" onclick=\"del()\">delete all</a>";
    		}
    	 out.write(HEAD);
   		 out.write(SCRIPT);
   		 out.write(String.format(PAGE, request.getSession().getServletContext().getContextPath()+"/uncode/cache",sbTask.toString(), kkey, match != null? match:"", delHtml, keyStr.toString(), valStr));
		}
		
	}
	
	
	
	public boolean checkAdminUser(String account, String password){
		if(StringUtils.isBlank(account) || StringUtils.isBlank(password)){
			return false;
		}
		String name = "admin";
		String pwd = "admin";
		if(account.equals(name) && password.equals(pwd)){
			return true;
		}
		return false;
	}
	
	class TimestampTypeAdapter implements JsonSerializer<Timestamp>, JsonDeserializer<Timestamp>{
	    public JsonElement serialize(Timestamp src, Type arg1, JsonSerializationContext arg2) {   
	    	DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");   
	        String dateFormatAsString = format.format(new Date(src.getTime()));   
	        return new JsonPrimitive(dateFormatAsString);   
	    }   
	  
	    public Timestamp deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {   
	        if (!(json instanceof JsonPrimitive)) {   
	            throw new JsonParseException("The date should be a string value");   
	        }   
	  
	        try {   
	        	DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");   
	            Date date = (Date) format.parse(json.getAsString());   
	            return new Timestamp(date.getTime());   
	        } catch (Exception e) {   
	            throw new JsonParseException(e);   
	        }   
	    }
	}

}

