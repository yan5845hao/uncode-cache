package cn.uncode.cache.store.redis.cluster;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;

import cn.uncode.cache.framework.ICache;
import cn.uncode.cache.framework.Level;
import cn.uncode.cache.framework.util.ByteUtil;
import cn.uncode.cache.framework.util.SerializeUtil;
import cn.uncode.cache.store.CacheStore;
import cn.uncode.cache.store.local.CacheTemplate;
import cn.uncode.cache.store.redis.JedisTemplate;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class RedisStore implements ICache<Object, Object> {

	
	private static final String KEY_USE_STORE_REGION = "defalut_redis_store_space";

	private JedisClusterCustom jedisCluster;
	
	private String storeRegion;
	
	private CacheStore cache;
	
	public JedisClusterCustom getJedisCluster() {
		return jedisCluster;
	}

	public void setJedisCluster(JedisClusterCustom jedisCluster) {
		this.jedisCluster = jedisCluster;
		JedisTemplate jedisTemplate = new JedisTemplate(jedisCluster);
		CacheTemplate cacheTemplate = new CacheTemplate(jedisTemplate, false, false);
		if(StringUtils.isNotBlank(storeRegion)){
			cache = new CacheStore(storeRegion, cacheTemplate);
		}else{
			cache = new CacheStore(KEY_USE_STORE_REGION, cacheTemplate);
		}
	}
	
	public void setStoreRegion(String storeRegion) {
		this.storeRegion = storeRegion;
		if(cache != null){
			cache.setStoreRegion(storeRegion);
		}
	}

	@Override
	public Object get(Object key) {
		return cache.get(key);
	}

	@Override
	public Object get(Object key, Level level) {
		return cache.get(key, level);
	}

	@Override
	public void put(Object key, Object value) {
		cache.put(key, value);
	}

	@Override
	public void put(Object key, Object value, Level level) {
		cache.put(key, value, level);
		
	}

	@Override
	public void put(Object key, Object value, int expireTime) {
		cache.put(key, value, expireTime);
	}

	@Override
	public void put(Object key, Object value, int expireTime, Level level) {
		cache.put(key, value, expireTime, level);
		
	}

	@Override
	public Object putIfAbsent(Object key, Object value) {
		return cache.putIfAbsent(key, value);
	}

	@Override
	public Object putIfAbsent(Object key, Object value, int expireTime) {
		return cache.putIfAbsent(key, value, expireTime);
	}

	@Override
	public void remove(Object key) {
		cache.remove(key);
		
	}

	@Override
	public void remove(Object key, Level level) {
		cache.remove(key, level);
		
	}

	@Override
	public void removeAll() {
		cache.removeAll();
		
	}

	@Override
	public void removeAll(Level level) {
		cache.removeAll(level);
		
	}

	@Override
	public int ttl(Object key) {
		return cache.ttl(key);
	}

	@Override
	public int ttl(Object key, Level level) {
		return cache.ttl(key, level);
	}

	@Override
	public List<Object> keys(String pattern) {
		return cache.keys(pattern);
	}

	@Override
	public List<Object> keys(String pattern, Level level) {
		return cache.keys(pattern, level);
	}

	@Override
	public void clear() {
		cache.clear();
		
	}

	@Override
	public int size() {
		return cache.size();
	}

	@Override
	public int size(Level level) {
		return cache.size(level);
	}

	@Override
	public boolean isExists(Object key) {
		return cache.isExists(key);
	}

	@Override
	public boolean isExists(Object key, Level level) {
		return cache.isExists(key, level);
	}

	@Override
	public Set<String> storeRegions() {
		return cache.storeRegionKeys(storeRegion);
	}

	@Override
	public Set<String> storeRegionKeys(String storeRegion) {
		return cache.storeRegionKeys(storeRegion);
	}

	@Override
	public Set<String> storeRegionKeys(String storeRegion, Level level) {
		return cache.storeRegionKeys(storeRegion, level);
	}

	@Override
	public List<Object> keysBySerialize(String byteToString) {
		List<Object> list = new ArrayList<Object>();
		if(StringUtils.isNotBlank(byteToString)){
			TreeSet<String> keys = innerKeys(byteToString);
			for (String str : keys) {
				if (StringUtils.isNotBlank(str)) {
					Object change = SerializeUtil.unserialize(ByteUtil.stringToByte(str));
					list.add(change==null?str:change);
				}
			}
		}
		return list;
	}
	
	private TreeSet<String> innerKeys(String pattern) {
		TreeSet<String> keys = new TreeSet<String>();
		Map<String, JedisPool> clusterNodes = jedisCluster.getClusterNodes();
		for (String key : clusterNodes.keySet()) {
			JedisPool jp = clusterNodes.get(key);
			Jedis connection = jp.getResource();
			try {
				keys.addAll(connection.keys(pattern));
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				connection.close();
			}
		}
		return keys;
	}

	@Override
	public Set<String> storeRegions(Level level) {
		return cache.storeRegionKeys(storeRegion, level);
	}

}
