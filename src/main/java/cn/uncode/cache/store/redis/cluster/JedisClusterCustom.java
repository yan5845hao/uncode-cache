package cn.uncode.cache.store.redis.cluster;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

//import cn.uncode.cache.framework.util.ByteUtil;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;

/**
 * redis cluster 支持byte[]
 * 
 * @author meff
 * @since 2016-4-21
 */
public class JedisClusterCustom extends JedisCluster {

	public JedisClusterCustom(Set<HostAndPort> jedisClusterNode, int timeout, int maxRedirections, String password, GenericObjectPoolConfig poolConfig) {
		super(jedisClusterNode, timeout, timeout, maxRedirections, password, poolConfig);
	}
	
	public Boolean exists(String key) {
		return super.exists(key);
	}
	
	public Boolean exists(byte[] bytes) {
		return super.exists(bytes);
	}
	
	public byte[] get(byte[] bytes) {
		return super.get(bytes);
	}

	public String set(byte[] bytes, byte[] object) {
		return super.set(bytes, object);
	}

	public void expire(byte[] bytes, Integer seconds) {
		super.expire(bytes, seconds);
	}

	/**
	 * 自定义keys方法
	 * 
	 * @author meff
	 * @return
	 */

	public Set<String> keys(String pattern) {
		Set<String> keys = new HashSet<>();
		Map<String, JedisPool> clusterNodes = getClusterNodes();
		for (String key : clusterNodes.keySet()) {
			JedisPool jp = clusterNodes.get(key);
			Jedis connection = jp.getResource();
			try {
				keys.addAll(connection.keys(pattern));
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				connection.close();
			}
		}
		return keys;
	}
	
	public Set<byte[]> keys(final byte[] pattern) {
		Set<byte[]> keys = new TreeSet<>();
		Map<String, JedisPool> clusterNodes = getClusterNodes();
		for (String key : clusterNodes.keySet()) {
			JedisPool jp = clusterNodes.get(key);
			Jedis connection = jp.getResource();
			try {
				keys.addAll(connection.keys(pattern));
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				connection.close();
			}
		}
		return keys;
	}

}
