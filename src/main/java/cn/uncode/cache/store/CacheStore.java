package cn.uncode.cache.store;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import cn.uncode.cache.framework.ICache;
import cn.uncode.cache.framework.Level;
import cn.uncode.cache.store.local.CacheTemplate;
import cn.uncode.cache.store.redis.CacheData;

public class CacheStore implements ICache<Object, Object> {

    private String        storeRegion;
    private CacheTemplate cacheTemplate;

    public CacheStore(String storeRegion, CacheTemplate cacheTemplate) {
        this.storeRegion = storeRegion;
        this.cacheTemplate = cacheTemplate;
    }
    
    @Override
	public void put(Object key, Object value, int expireTime) {
    	cacheTemplate.set(storeRegion, String.valueOf(key), value, expireTime);
	}

	@Override
	public void remove(Object key) {
		this.cacheTemplate.del(this.storeRegion, String.valueOf(key));
	}
	
	@Override
	public List<Object> keys(String pattern) {
		return keys(pattern, Level.Remote);
	}

	@Override
	public List<Object> keys(String pattern, Level level) {
		List<Object> keylist = new ArrayList<Object>();
		Set<String> keys = cacheTemplate.keys(storeRegion, pattern, level);
		keylist.addAll(keys);
		return keylist;
	}

	@Override
	public int size() {
		int sizeLocal = cacheTemplate.size(storeRegion, Level.Local);
		int sizeRemote = cacheTemplate.size(storeRegion, Level.Remote);
		return sizeLocal > sizeRemote ? sizeLocal : sizeRemote;
	}
	
	@Override
	public int size(Level level) {
		return cacheTemplate.size(storeRegion, level);
	}
	
	@Override
	public boolean isExists(Object key) {
		return isExists(key, Level.Remote);
	}

	@Override
	public boolean isExists(Object key, Level level) {
		Object val = get(key, level);
		if(null != val){
			return true;
		}
		return false;
	}

	@Override
	public Object get(Object key) {
		Object val = this.cacheTemplate.get(this.storeRegion, String.valueOf(key));
		return val;
	}

	@Override
	public void put(Object key, Object value) {
		this.cacheTemplate.set(this.storeRegion, String.valueOf(key), value);
	}

	@Override
	public void clear() {
		this.cacheTemplate.rem(this.storeRegion);
	}
	
	public Object putIfAbsent(Object key, Object value) {
		Object result = this.cacheTemplate.setIfAbsent(this.storeRegion, String.valueOf(key), value);
		return result;
	}

	@Override
	public Object get(Object key, Level level) {
		Object result = cacheTemplate.get(storeRegion, String.valueOf(key), level);
		return result;
	}

	@Override
	public void put(Object key, Object value, Level level) {
		cacheTemplate.set(storeRegion, String.valueOf(key), value, level);
	}

	@Override
	public void put(Object key, Object value, int expireTime, Level level) {
		cacheTemplate.set(storeRegion, String.valueOf(key), value, expireTime, level);
	}

	@Override
	public Object putIfAbsent(Object key, Object value, int expireTime) {
		Object result = this.cacheTemplate.setIfAbsent(this.storeRegion, String.valueOf(key), value, expireTime);
		return result;
	}

	@Override
	public void remove(Object key, Level level) {
		cacheTemplate.del(storeRegion, String.valueOf(key), level);
	}

	@Override
	public void removeAll() {
		cacheTemplate.rem(storeRegion);
	}

	@Override
	public void removeAll(Level level) {
		cacheTemplate.rem(storeRegion, level);
	}

	@Override
	public int ttl(Object key) {
		int result = cacheTemplate.ttl(storeRegion, String.valueOf(key));
		return result;
	}

	@Override
	public int ttl(Object key, Level level) {
		int result = cacheTemplate.ttl(storeRegion, String.valueOf(key), level);
		return result;
	}
	
	@Override
	public Set<String> storeRegions() {
		return storeRegions(Level.Remote);
	}


	@Override
	public Set<String> storeRegions(Level level) {
		return cacheTemplate.localNames(level);
	}

	@Override
	public Set<String> storeRegionKeys(String storeRegion) {
		return cacheTemplate.keys(storeRegion, Level.Remote);
	}
	
	
	public Set<String> storeRegionKeys(String storeRegion, Level level){
		return cacheTemplate.keys(storeRegion, level);
	}

	
	 public List<CacheData> fetch(String key){
		 return cacheTemplate.fetch(storeRegion, key);
	 }

	@Override
	public List<Object> keysBySerialize(String byteToString) {
		// TODO Auto-generated method stub
		return null;
	}

	public void setStoreRegion(String storeRegion) {
		this.storeRegion = storeRegion;
	}

	public CacheTemplate getCacheTemplate() {
		return cacheTemplate;
	}

}


