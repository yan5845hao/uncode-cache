/**
 * 
 */
package cn.uncode.cache.framework;

import java.util.List;
import java.util.Set;

/**
 * @author juny.ye
 * @email  juny.ye@ksudi.com
 * 
 * @param <K> key of cache
 * @param <V> value of cache
 * 
 * 2015年4月23日
 */
public interface ICache<K, V> {

    public enum Operator {
        SET, GET, DEL, REM, CLS
    }
	
	/**
	 * 获取数据【Local&Remote】<br/>
	 * 启用本地缓存的情况下先读取Local,再读Remote；否则直接读取Remote
	 * 
	 * @param key 缓存key,默认会加项目前缀
	 * @return 缓存值
	 */
	public V get(K key);
	
	/**
	 * 获取指定级别的数据【Local|Remote】
	 * 
	 * @param key 缓存key，默认会加项目前缀
	 * @param level 缓存级别，Local或Remote
	 * @return 缓存值，本地缓存没有启用，读取Local级别返回null
	 */
	public V get(K key, Level level);

	/**
	 * 设置数据，如果数据已经存在，则覆盖，如果不存在，则新增【Local&Remote】<b/>
	 * 启用Local缓存并开启Local缓存同步：同时存Local和Remote，会发送set命令<br/>
	 * 启用Local缓存未开启Local缓存同步：同时存Local和Remote，发送del命令<br/>
	 * 未启用Local缓存：只存Remote<br/>
	 * 
	 * @param key 缓存key，默认会加项目前缀
	 * @param value 缓存值
	 */
	public void put(K key, V value);
	
	/**
	 * 设置指定级别数据，如果数据已经存在，则覆盖，如果不存在，则新增【Local|Remote】<b/>
	 * Local级别：
	 * 启用Local缓存时存Local级缓存，未启用Local缓存时没有任何操作<br/>
	 * Remote级别：存Remote级缓存<br/>
	 * 
	 * @param key 缓存key，默认会加项目前缀
	 * @param value 缓存值
	 */
	public void put(K key, V value, Level level);

	/**
	 * 设置数据并指定过期时间，如果数据已经存在，则覆盖，如果不存在，则新增【Local&Remote】<br/>
	 * 启用Local缓存并开启Local缓存同步：同时存Local和Remote，会发送set命令<br/>
	 * 启用Local缓存未开启Local缓存同步：同时存Local和Remote，发送del命令<br/>
	 * 未启用Local缓存：只存Remote<br/>
	 * 
	 * @param key 缓存key，默认会加项目前缀
	 * @param value 缓存值
	 * @param expireTime 数据的有效时间（绝对时间），单位秒
	 */
	public void put(K key, V value, int expireTime);
	
	
	/**
	 * 设置指定级别数据并指定过期时间，如果数据已经存在，则覆盖，如果不存在，则新增【Local|Remote】<br/>
	 * Local级别：
	 * 启用Local缓存时存Local级缓存，未启用Local缓存时没有任何操作<br/>
	 * Remote级别：存Remote级缓存<br/>
	 * 
	 * @param key 缓存key，默认会加项目前缀
	 * @param value 缓存值
	 * @param expireTime 数据的有效时间（绝对时间），单位秒，0代表永久有效
	 */
	public void put(K key, V value, int expireTime, Level level);
	
	/**
	 * 覆盖原有缓存，不存在不作任何操作并返回空【Local&Remote】<br/>
	 * 启用Local缓存：同时存Local和Remote，会发送set命令<br/>
	 * 未开启Local缓存：只存Remote<br/>
	 * 其他参考put(K key, V value)方法
	 * @param key 缓存key，默认会加项目前缀
	 * @param value 缓存值
	 * @return 返回值为null
	 */
    public Object putIfAbsent(K key, Object value);
    
	/**
	 * 覆盖原有缓存并指定过期时间，不存在不作任何操作并返回空【Local|Remote】<br/>
	 * Local级别：
	 * 启用Local缓存时存Local级缓存，未启用Local缓存时没有任何操作<br/>
	 * Remote级别：存Remote级缓存<br/>
	 * 其他参考put(K key, V value, int expireTime)方法
	 * @param key 缓存key，默认会加项目前缀
	 * @param value 缓存值
	 * @return 返回值为null
	 */
    public Object putIfAbsent(K key, Object value, int expireTime);

	/**
	 * 删除指定key对应的数据【Local&Remote】<br/>
	 * 启用Local缓存：同时删除Local和Remote，会发送del命令<br/>
	 * 未启用Local缓存：只删除Remote<br/>
	 * 
	 * @param key 缓存key，默认会加项目前缀
	 */
	public void remove(K key);

	/**
	 * 删除指定级别key对应的数据【Local|Remote】<br/>
	 * Local级别：启用Local缓存时删除Local，会发送del命令；未启用Local缓存不作任何操作<br/>
	 * Remote级别：删除Remote<br/>
	 * 
	 * @param key 缓存key，默认会加项目前缀
	 * @param level 缓存级别，Local或Remote
	 */
	public void remove(K key, Level level);
	
	
	/**
	 * 删除当前项目前缀或缓存分区的所有缓存【Local&Remote】<br/>
	 * 启用Local缓存：同时删除Local和Remote，会发送del命令<br/>
	 * 未启用Local缓存：删除Remote<br/>
	 */
	public void removeAll();
	
	/**
	 * 删除当前项目前缀或缓存分区的指定级别下所有缓存【Local|Remote】<br/>
	 * Local级别：启用Local缓存时删除Local，会发送del命令；未启用Local缓存不作任何操作<br/>
	 * Remote级别：删除Remote<br/>
	 * 
	 * @param level 缓存级别，Local或Remote
	 */
	public void removeAll(Level level);
	
	
	/**
     * 获取缓存过期剩余时间.单位为秒【Local&Remote】<br/>
     * 启用Local缓存：同时读取Local和Remote，如果Local不存在，读取Remote并返回；如果Local存在直接返回，此场景要读Remote时，使用ttl(K key, Level.Remote)<br/>
	 * 未启用Local缓存：读取Remote数据返回<br/>
     * 0：永久；
     * -1：不存在；
     * >0：过期剩余时间
     */
    public int ttl(K key);
    
	/**
     * 获取缓存过期剩余时间.单位为秒【Local|Remote】<br/>
	 * Local级别：启用Local缓存时读取Local；未启用Local缓存返回-1<br/>
	 * Remote级别：读取Remote数据<br/>
     * 0：永久；
     * -1：不存在；
     * >0：过期剩余时间
     */
    public int ttl(K key, Level level);
    
    
    /**
     * 获取前项目前缀或缓存分区的所有匹配的key【Remote】<br/>
     * 返回当前项目前缀Remote级所有匹配的key,不包括Local级别<br/>
     * 建议使用 keys(String pattern, Level level)
     * <pre>
	 * 例：{"uncodekey":"mytest"}
	 * 可以匹配的pattern值为：
	 * 可带星：**code**或uncode**或**key
	 * 不带星：code或uncode或key
	 * </pre>
     * 
     * @param pattern 匹配字符串，为null时匹配所有
     * @return 匹配的key集合
     */
    public List<K> keys(String pattern);
	
	/**
     * 获取前项目前缀或缓存分区的指定级别下所有匹配的key【Local|Remote】<br/>
     * Local级别：启用Local缓存时返回所有匹配的key；未启用Local缓存返回Null<br/>
	 * Remote级别：返回Remote缓存所有匹配的key<br/>
	 * <pre>
	 * 例：{"uncodekey":"mytest"}
	 * 可以匹配的pattern值为：
	 * 可带星：**code**或uncode**或**key
	 * 不带星：code或uncode或key
	 * </pre>
     * 
     * @param pattern 匹配字符串,为null时匹配所有
     * @param level 缓存级别，Local或Remote
     * @return 匹配的key集合
     */
    public List<K> keys(String pattern, Level level);

	/**
	 * 清除当前项目前缀或缓存分区的所有缓存【Local&Remote】<br/>
	 * 同removeAll()<br/>
	 */
	public void clear();

	/**
	 * 获取当前项目前缀或缓存分区的所有缓存数量【Local&Remote】<br/>
	 * 启用Local缓存：同时读取Local和Remote，返回数量最大的<br/>
	 * 未启用Local缓存：返回当前项目前缀Remote级缓存数量<br/>
	 * 建议使用size(Level level)
	 * 
	 * @return 数量
	 */
	public int size();
	
	/**
	 * 获取指定级别当前项目前缀或缓存分区的所有缓存数量【Local|Remote】<br/>
	 * Local级别：启用Local缓存时返回当前项目前缀该级缓存数量；未启用Local缓存返回0<br/>
	 * Remote级别：返回Remote级当前项目前缀缓存数量<br/>
	 * 
	 * @param level 缓存级别，Local或Remote
	 * @return 数量
	 */
	public int size(Level level);
	
	/**
	 * 只判断别当前项目前缀或缓存分区Remote级别的key是否存在【Remote】
	 * 
	 * @param key 缓存key，默认会加项目前缀
	 * @return {boolean}
	 */
	public boolean isExists(K key);
	
	/**
	 * 判断别当前项目前缀或缓存分区的key是否存在【Local|Remote】<br/>
	 * @param key 缓存key，默认会加项目前缀
	 * @param level 缓存级别，Local或Remote
	 * @return {boolean}
	 */
	public boolean isExists(K key, Level level);
	
	
	/**
	 * 获取Local级所有项目前缀或缓存分区名称集合【Local】<br/>
	 * 考虑到性能问题，Remote级别不实现
	 * @return 名称集合
	 */
	Set<String> storeRegions();
	
	Set<String> storeRegions(Level level);
	
	
	/**
	 * 获取当前项目前缀或缓存分区的所有缓存key【Remote】<br/>
	 * 返回当前项目前缀Remote级所有key,不包括Local级别<br/>
	 * 
	 * 获取的是Remote的key,建议使用storeRegionKeys(Level.Remote)
	 * 
	 * @return key的set集合
	 */
	Set<String> storeRegionKeys(String storeRegion);
	
	/**
	 * 获取指定级别当前项目前缀或缓存分区的所有缓存key【Local|Remote】<br/>
	 * Local级别：启用Local缓存时返回当前项目前缀所有级缓key；未启用Local缓存返回null<br/>
	 * Remote级别：返回Remote级当前项目前缀所有缓存key<br/>
	 * 
	 * @param level 缓存级别，Local或Remote
	 * @return key的set集合
	 */
	Set<String> storeRegionKeys(String storeRegion, Level level);
	
	/**
	 * 设置存储分区
	 * @param storeRegion
	 */
	void setStoreRegion(String storeRegion);

	@Deprecated
	List<Object> keysBySerialize(String byteToString);

}
