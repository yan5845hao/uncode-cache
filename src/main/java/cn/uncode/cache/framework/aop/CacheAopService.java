package cn.uncode.cache.framework.aop;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.core.MethodParameter;
import org.springframework.core.io.InputStreamSource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.uncode.cache.CacheManager;
import cn.uncode.cache.CacheUtils;
import cn.uncode.cache.annotation.Cache;
import cn.uncode.cache.framework.util.CacheCodeUtil;
import cn.uncode.cache.framework.util.ClassUtil;

@Aspect
public class CacheAopService {

	@Around(value = "execution(* *.*(..)) && @annotation(cn.uncode.cache.annotation.Cache)")
	public Object aroundMethod(ProceedingJoinPoint point) throws Throwable {
		MethodSignature joinPointObject = (MethodSignature) point.getSignature();
		Method method = joinPointObject.getMethod();
		boolean flag = method.isAnnotationPresent(Cache.class);
		if(flag) {
			Object[] args = point.getArgs();
			final Map<String, Object> paraMap = new HashMap<>(16);
			for (int i = 0; i < args.length; i++) {
				MethodParameter methodParam = ClassUtil.getMethodParameter(method, i);
				PathVariable pathVariable = methodParam.getParameterAnnotation(PathVariable.class);
				if (pathVariable != null) {
					continue;
				}
				RequestBody requestBody = methodParam.getParameterAnnotation(RequestBody.class);
				Object object = args[i];
				// 如果是body的json则是对象
				if (requestBody != null && object != null) {
					paraMap.putAll(BeanMap.create(object));
				} else {
					RequestParam requestParam = methodParam.getParameterAnnotation(RequestParam.class);
					String paraName;
					if (requestParam != null && StringUtils.isNotBlank(requestParam.value())) {
						paraName = requestParam.value();
					} else {
						paraName = methodParam.getParameterName();
					}
					paraMap.put(paraName, object);
				}
			}
			// 处理 参数
			List<String> needRemoveKeys = new ArrayList<>(paraMap.size());
			paraMap.forEach((key, value) -> {
				if (value instanceof HttpServletRequest) {
					needRemoveKeys.add(key);
					paraMap.putAll(((HttpServletRequest) value).getParameterMap());
				} else if (value instanceof HttpServletResponse) {
					needRemoveKeys.add(key);
				} else if (value instanceof InputStream) {
					needRemoveKeys.add(key);
				} else if (value instanceof MultipartFile) {
					String fileName = ((MultipartFile) value).getOriginalFilename();
					paraMap.put(key, fileName);
				} else if (value instanceof InputStreamSource) {
					needRemoveKeys.add(key);
				} else if (value instanceof WebRequest) {
					needRemoveKeys.add(key);
					paraMap.putAll(((WebRequest) value).getParameterMap());
				}
			});
			needRemoveKeys.forEach(paraMap::remove);
			String cacheKey = CacheCodeUtil.getCacheAdapterKey(CacheManager.getStoreRegion(), joinPointObject.getDeclaringTypeName(), 
					method.getName(), method.getParameterTypes(), paraMap);
			Cache cache = method.getAnnotation(Cache.class);
			Object value = CacheUtils.getCache().get(cacheKey, cache.level());
			if(null == value) {
				value = point.proceed();
				CacheUtils.getCache().put(cacheKey, value, cache.expired(), cache.level());
			}
			return value;
		}else {
			return point.proceed();
		}
	}


}
